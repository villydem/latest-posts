import { Component, Input } from '@angular/core';
import { Author } from '../model/author';


@Component({
  selector: 'author',
  templateUrl: './author.component.html'
})

export class AuthorComponent {

	@Input()
	author:  Author;
}
