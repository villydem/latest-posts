import { PostListComponent } from './post-list.component';

describe('PostListComponent', () => {
beforeEach(function() {
	this.app = new PostListComponent();
});

it('should sort post items by id', function() {
	this.app.postItems = [{post:{id:1}},{post:{id:2}}];
	this.app.sortList();
	expect(this.app.postItems).toEqual([{post:{id:2}},{post:{id:1}}]);
});

it('should capitalize first letter of title', function() {
  	this.app.postItems = [{
  		post:{
  			title: "test capitalize"
  		}
  	}];
 	this.app.capitalize();
   	expect(this.app.postItems[0].post.title).toEqual("Test capitalize");
	});

});
