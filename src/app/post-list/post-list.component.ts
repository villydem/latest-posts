import { Component, OnInit } from '@angular/core';

import { Post } from '../model/post';
import { PostItem } from '../model/post-item';
import { AuthorService } from  '../services/post-author.service';
import { PostDataService } from '../services/post-data.service';

@Component({
  selector: 'post-list',
  templateUrl: './post-list.component.html',
  providers: [AuthorService,PostDataService]
})

export class PostListComponent implements OnInit {
	postItems: PostItem[] = [];
	visiblePosts: PostItem[] = [];
	postItemsLimit: number = 30;
	visiblePostsCount: number = 10;

	constructor (private postService: PostDataService, private authorService: AuthorService) {}

	ngOnInit() { 
		this.getAllPosts();
	}

	getAllPosts() {	
	   return this.postService.getPosts()
	                 .subscribe(
	                   success => { 
	                   		let posts = (success.length > this.postItemsLimit) ?
	                   					success.slice(success.length - this.postItemsLimit, success.length) : 
	                   					success;
	                   		this.createPostList(posts);
	                   });
	}

	getAuthor(postItem: PostItem) {
		if (postItem.author) {
			return;
		}

		let foundAuthor = this.postItems.find(function(item) {
			return item.author && item.author.id === postItem.post.userId;
		});

		if (foundAuthor) {
			postItem.author = foundAuthor.author;
			return;
		}

		return this.authorService.getAuthor(postItem.post.userId)
	                 .subscribe(
	                   success => postItem.author = success);
	}

	createPostList(posts : Post[]) {

		this.postItems = posts.map(function(post){
			return new PostItem(post);
		});
		
		this.sortList();
		this.capitalize();
		this.visiblePosts = this.postItems.slice(0, this.visiblePostsCount);
	}

	sortList() {
		this.postItems.sort(function(a:PostItem, b:PostItem) {
			return (b.post.id - a.post.id);
		});
	}

	capitalize() {
		this.postItems.forEach(function(postItem) {
			let firstLetter = postItem.post.title.charAt(0).toUpperCase();
			postItem.post.title = firstLetter + postItem.post.title.substring(1);
		});
	}

	pageChanged($event) {
		this.visiblePosts = this.postItems.slice($event.page * this.visiblePostsCount - this.visiblePostsCount,
										  $event.page * this.visiblePostsCount);
	}

}
