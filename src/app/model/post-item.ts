import { Post } from './post';
import { Author } from './author';

export class PostItem {
	post: Post ;
	author: Author ;

	constructor(post: Post) {
		this.post = post;
	}
}