import { Injectable } from '@angular/core';
import { Http, Response }   from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { BaseService } from './base.service';
import { Post } from '../model/post';

@Injectable()
export class PostDataService extends BaseService {
	private postsUrl = 'http://jsonplaceholder.typicode.com/posts' ;

	constructor (private http: Http) {
		super();
	}

	getPosts(): Observable<Post[]> {
		return this.http.get(this.postsUrl)
	                .map(this.extractData)
	                .catch(this.handleError);
	}
}
