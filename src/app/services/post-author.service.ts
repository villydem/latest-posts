import { Injectable } from '@angular/core';
import { Http, Response }   from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { BaseService } from './base.service';
import { Author } from '../model/author';


@Injectable()
export class AuthorService extends BaseService {
	private authorUrl = ' http://jsonplaceholder.typicode.com/users' ;

	constructor (private http: Http) {
		super();
	}

	getAuthor(id:number): Observable<Author> {
		return this.http.get(this.authorUrl + '/' + id)
	                .map(this.extractData)
	                .catch(this.handleError);
	}
}
