import { LatestPostsPage } from './app.po';

describe('latest-posts App', () => {
  let page: LatestPostsPage;

  beforeEach(() => {
    page = new LatestPostsPage();
  });

  it('should display message saying latest posts', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Latest posts');
  });
});
